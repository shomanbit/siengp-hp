﻿<font size="+1">
<section>
	<h2>アクセス</h2>
	<p><FONT color="ff4500"><big>・会場へのアクセス</big></FONT>
	<br>会場は<b><u>東京工業大学　大岡山キャンパス　西9号館2F　コラボレーションルーム</u></b>です。
	<br>会場は東京工業大学の正門から入ってそのまま西側の坂道を下り、最初の曲り道を左に曲がり、すぐ右手にある西9号館です。
　　　　<br>第一食堂から出て、右手の坂道を下ってすぐ右手に見える西9号館の入り口からも会場に向かえます。
	<br>
	<br><img src="./images/access.png">
	<br>
	<br>
	<br><big><FONT color="ff4500">・最寄り駅へのアクセス</FONT></big>
	<br><b>東急目黒線・大井町線　大岡山駅　正門まで徒歩1分</b>
	<blockquote style="MARGIN-RIGHT: 0px" dir="ltr">
	<p style="font-size: 1.3em;">最寄駅　：　東急目黒線・東急大井町線　大岡山駅</p>
	<p><iframe width="520" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/?ie=UTF8&amp;vpsrc=0&amp;brcurrent=3,0x6018f532b667c937:0x1d417f766767de3e,1&amp;ll=35.606004,139.684017&amp;spn=0.008723,0.011137&amp;z=16&amp;output=embed"></iframe><br /><small><a href="http://maps.google.co.jp/?ie=UTF8&amp;vpsrc=0&amp;brcurrent=3,0x6018f532b667c937:0x1d417f766767de3e,1&amp;ll=35.606004,139.684017&amp;spn=0.008723,0.011137&amp;z=16&amp;source=embed" style="color:#0000FF;text-align:left">大きな地図で見る</a></small>

	<br>
	<br>学校の所在地は
　　　　<br><b>〒152-8550
        <br>東京都目黒区大岡山2-12-1</b>
	<br>です。
	<br>大岡山駅までの交通案内は<a href="http://www.titech.ac.jp/about/campus/index.html">こちら</a>
	（東京工業大学ホームページへ移動します）
	</p></blockquote>
	</p>
	
</section>
</font>