      <div id="content">

        <!-- insert the page content here -->
	<h1>パネリスト<span style="font-size: 0.8em;"><B>（全員、東工大出身の方です！！）</B></span></h1>

	<p><img src="./style/sato.jpg" alt="佐藤直良" /><br><br>
	<span style="font-size: 1.6em; color: Black;">佐藤 直良 氏</span><br><br>
国土交通事務次官<br>
　1952年横浜生まれ。東京工業大学工学部土木工学科卒、同大学院理工学研究科土木工学専攻修了。
1977年旧建設省に入省し、現在に至る。
これまでの間、佐賀市助役、四国地方整備局河川部長、独立行政法人水資源機構経営企画部長、国土交通省大臣官房技術調査課長、大臣官房技術審議官、中部地方整備局長、河川局長、技監等を歴任。
<br><br><br>
</p>

	<p><img src="./style/ido.jpg" alt="井戸清人" /><br><br>
	<span style="font-size: 1.6em; color: Black;">井戸 清人 氏</span><br><br>
㈱国際経済研究所副理事長<br>
　1950年東京生まれ。
東京工業大学理学部数学科卒業、ドイツのザールランド大学留学。
1973年大蔵省（現財務省）入省。国際金融局を中心に勤務し、副財務官、国際局審議官、国際局次長を経て、2004年国際局長就任。
この間、在フランクフルト日本総領事館領事、米州開発銀行財務局次長、在米国日本大使館公使として、海外にも勤務。
1980年代は、東京オフショア市場創設や円の国際化など我が国金融市場の国際化に努力。1990年代には、アジア諸国の経済開発など開発金融の分野に従事。
1997年から1998年のアジア通貨危機においては、300億ドルの資金支援やアジア地域金融協力構想に向けて東アジア諸国と協力した。
2006年日本銀行理事に就任し、2010年任期満了により退任。
2011年4月、トヨタ自動車のシンクタンクである国際経済研究所の副理事長に就任し、現在に至る。
<br><br><br>
</p>
	
	<p><img src="./style/maruyama.jpeg" alt="丸山剛司" /><br><br>
	<span style="font-size: 1.6em; color: Black;">丸山 剛司 氏</span><br><br>
中央大学大学院公共政策研究科特任教授、東京工業大学副学長（非常勤）<br>
　1951年東京生まれ。東工大工学部電気工学科、同大学院理工学研究科電子工学専攻修士、ハーバード大学大学院ケネディスクール公共政策修士。
1975年から2011年まで科学技術庁、文部科学省、内閣府等に勤務。
課長時代は、宇宙、海洋開発、エネルギー、ナノテクノロジーなど数々の科学技術プロジェクトの企画・立案・推進に従事した。
文部科学省発足後は、文化庁審議官、総括審議官、科学技術学術・政策局長や内閣府政策統括官（科学技術・イノベーション担当）、内閣審議官（宇宙戦略本部）など。
2011年4月から中央大学大学院公共政策研究科にて、エネルギー政策、科学技術・イノベーション政策、意思決定などの講義を担当。
公務員時代は、科学技術政策の基本である科学技術基本計画の策定に携わるとともに、日米スパコン摩擦をめぐる国際交渉、海洋探査船「ちきゅう」の立ち上げに関しての多国間交渉をはじめ、原子力関係法、著作権法の改正等が思い出に残る。
<br><br><br>
</p>
	
	<p><img src="./style/terasaki.JPG" alt="寺崎明" /><br><br>
	<span style="font-size: 1.6em; color: Black;">寺崎 明 氏</span><br><br>
㈱野村総合研究所顧問、東京工業大学客員教授<br>
　1952年東京生まれ。東工大工学部電子物理工学科卒、同大学院理工学研究科電子物理工学専攻修了。
1976年から2010年まで旧郵政省・総務省に勤務、郵政省では電気通信局移動通信課長・通信政策局技術政策課長・北陸電気通信監理局長、総務省では電気通信事業部長・政策統括官・総合通信基盤局長・総務審議官などを務める。
途中内閣官房の内閣審議官（ＩＴ担当室）も 務め、一貫して我が国の情報通信政策等の立案・実施に従事。
2010年総務省退官後、同省顧問を務めた後、㈱野村総合研究所及び東工大において情報通信政策に関わる業務・教育に従事。
特に、ＮＴＴの民営化・競争政策の導入、日米政府間の携帯電話に関わる貿易摩擦の解消、携帯電話方式の国際標準化と周波数の獲得、中国へのＰＨＳの導入（１億台普及）、ＪＧＮ(日本縦断広帯域光ファイバーテストベッド)の整備、青少年に対するインターネット上の違法有害情報対策の実施、情報通信技術のグローバル化（例えば南米大陸での地デジ日本規格の席捲）等の業務が記憶に残る。
<br><br><br>
</p>

</div>
