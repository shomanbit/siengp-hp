      <div id="content">

        <!-- insert the page content here -->
<h1>アクセス</h1>
<h2>会場</h2>
<blockquote style="MARGIN-RIGHT: 0px" dir="ltr">
<p style="font-size: 1.3em;"><font color="Black">
東京工業大学　大岡山キャンパス　西８号館10F　情報理工学研究科会議室</font><br><br>
<img src="./style/access.jpg" alt="" />
</p></blockquote>
<h2>会場へのアクセス</h2>
<blockquote style="MARGIN-RIGHT: 0px" dir="ltr">
<p style="font-size: 1.3em;"><font color="Black">最寄駅　：　東急目黒線・東急大井町線　大岡山駅（徒歩５分）</font></p>
<p><iframe width="520" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/?ie=UTF8&amp;vpsrc=0&amp;brcurrent=3,0x6018f532b667c937:0x1d417f766767de3e,1&amp;ll=35.606004,139.684017&amp;spn=0.008723,0.011137&amp;z=16&amp;output=embed"></iframe><br /><small><a href="http://maps.google.co.jp/?ie=UTF8&amp;vpsrc=0&amp;brcurrent=3,0x6018f532b667c937:0x1d417f766767de3e,1&amp;ll=35.606004,139.684017&amp;spn=0.008723,0.011137&amp;z=16&amp;source=embed" style="color:#0000FF;text-align:left">大きな地図で見る</a></small>

<br>
大岡山駅までの交通案内は<a href="http://www.titech.ac.jp/about/campus/index.html">こちら</a>
（東京工業大学ホームページへ移動します）
</p></blockquote>

      </div>
