      <div id="content">

        <!-- insert the page content here -->

        <h1>お問い合わせ</h1>

        <p style="MARGIN-RIGHT: 0px" dir="ltr">本特別講義に関するお問い合わせは、<br>
	件名を<font color="Black">「第７回シンポジウム」</font>にし、<br>
	<font color="Black">「お名前」、「お問い合わせ内容」</font>を明記の上、<br>
	以下のアドレスにメールにてお願いいたします。<br><br>
	アドレス：<font color="Black">saito.h.ag-symposium12アットマークm.titech.ac.jp</font><br>
　　　　　（「アットマーク」を「@」(半角)に置き換えてください。）<br><br>
	記入いただいた個人情報は、お問い合わせへの対応に限って利用いたします。</p>

<p></p>
<p>
東京工業大学　理工系学生能力発見・開発プロジェクト<br>
〒152-8552　東京都目黒区大岡山2-12-1<br>
TEL： 03-5734-3230 （お問い合わせは基本的にメールでお願いいたします）
</p>


      </div>
