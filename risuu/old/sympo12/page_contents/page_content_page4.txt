      <div id="content">

        <!-- insert the page content here -->

	<h1>申し込み</h1>
	
	<p><font color="Black">人数把握のため、本シンポジウムでは事前に申し込みをしていただきます。
	<br>以下の申し込みフォームに必要事項を記入し、送信ボタンを押してください。</font>
	<br>複数人でのご参加の場合必ずお一人ずつお申込ください。
	<br>その際必ず代表者名や保護者名でなく、参加者名で登録してください　同名の場合、二重の申込と判断されることがあります。
	</p>
	<p><font color="Black">申込者数が定員（100名ほど）に達し次第、事前申込を終了させていただきます。</font>
	<br>当日参加も可能ですが、定員数を上回った場合、事前申し込みしていただいた方を優先させていただきます。
	ご了承のうえお越しください。</p>
	
	<!--	
	<blockquote style="MARGIN-RIGHT: 0px" dir="ltr">
	<iframe src="https://docs.google.com/spreadsheet/embeddedform
	?formkey=dFhsV3hvN0hPMkdWbUpBQ2RvM0lYUGc6MQ" width="550" height="1000" frameborder="0" marginheight="0" marginwidth="0">
	読み込み中...</iframe>
	</blockquote>
	--!>
	
	<p><font color="Red"><B>本シンポジウムはすでに終了致しました。<br>
	ありがとうございました。</B></font>
	</p>
	
      </div>
