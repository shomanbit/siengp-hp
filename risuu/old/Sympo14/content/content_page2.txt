<font size="+1">
<section>
	<h2>講師紹介</h2>
	<p><b>水本哲弥副学長
	<br>東京工業大学大学院理工学研究科電気電子工学専攻
	<br><img src="./images/mizumoto.jpg"height="200">
	<br>	
	<br>本川達雄名誉教授
	<br>東京工業大学教授(1991~2014)
	<br>2014年4月より、東京工業大学名誉教授
	<br><img src="./images/motokawa.jpg"height="200">
	<br>
	<br>上田紀行教授
	<br>東京工業大学リベラルアーツセンター（大学院社会理工学研究科価値システム専攻兼任）
	<br><img src="./images/ueda.jpg"height="200">
	<br>
	<br>伊藤亜紗准教授
	<br>東京工業大学リベラルアーツセンター（大学院社会理工学研究科価値システム専攻兼任）
	<br><img src="./images/ito.jpg"height="200">
	</b>
	
	

	</p>
	
</section>
</font>