      <div id="content">

        <!-- insert the page content here -->

        <h1>お問い合わせ</h1>

        <p style="MARGIN-RIGHT: 0px" dir="ltr">本特別講義に関するお問い合わせは、以下の事項を明記の上、メールにてお願いいたします。
</p>
<blockquote style="MARGIN-RIGHT: 0px" dir="ltr">

<p>お名前：<br>
ふりがな：<br>
申込有無：<br>
お問い合わせ内容：<br>
返信先メールアドレス*：<br>
(*送信元アドレスと異なるアドレスへの返信をご希望の場合のみ）</p>

<p>件名：第６回特別講義お問い合わせ<br>
送信先：<img border="0" align="top" alt="" src="./page_contents/mail.png" width="207"></p></blockquote>
<p>記入いただいた個人情報は、お問い合わせへの対応に限って利用いたします。</p>

<p></p>
<p>
東京工業大学　理工系学生能力発見・開発プロジェクト<br>
〒152-8552　東京都目黒区大岡山2-12-1<br>
TEL： 03-5734-3230 （お問い合わせはメールでお願いいたします）
</p>


      </div>
