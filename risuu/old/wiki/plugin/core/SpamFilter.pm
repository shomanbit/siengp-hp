###############################################################################
#
# スパム投稿をフィルタリングするフックプラグイン。
#
###############################################################################
package plugin::core::SpamFilter;
#use strict;
#==============================================================================
# コンストラクタ
#==============================================================================
sub new {
	my $class = shift;
	my $self = {};
	return bless $self,$class;
}
#==============================================================================
# フックメソッド
#==============================================================================
sub hook {
	my $self = shift;
	my $wiki = shift;
	my $cgi  = $wiki->get_CGI();
	
	my $content = $cgi->param("content");
	my $spam    = Util::load_config_text($wiki,"spam.dat");
	foreach my $spam_line (split(/\n/,$spam)){
		chomp($spam_line);
		if(index($content,$spam_line)!=-1){
			$wiki->redirect($cgi->param("page"));
		}
	}
}

1;

