############################################################
#
# ページの名称を変更するためのフォームを出力します。
#
############################################################
package plugin::rename::RenameForm;
use strict;
#===========================================================
# コンストラクタ
#===========================================================
sub new {
	my $class = shift;
	my $self = {};
	return bless $self,$class;
}

#===========================================================
# ヘルプを表示します。
#===========================================================
sub editform {
	my $self = shift;
	my $wiki = shift;
	my $cgi  = $wiki->get_CGI();
	my $page = $cgi->param("page");
	
	# ページが存在する場合だけフォームを表示
	if($wiki->page_exists($page)){
		my $time = $wiki->get_last_modified($page);
		return "<h2>リネーム・コピー</h2>".
		       "<form method=\"post\" action=\"".$wiki->config('script_name')."\">\n".
		       "  <input type=\"text\" name=\"newpage\" size=\"40\" value=\"".&Util::escapeHTML($page)."\">\n".
		       "  <br>\n".
		       "  <input type=\"radio\"  name=\"do\" value=\"move\" checked>リネーム\n".
		       "  <input type=\"radio\"  name=\"do\" value=\"movewm\">メッセージを残してリネーム\n".
		       "  <input type=\"radio\"  name=\"do\" value=\"copy\">コピー\n".
		       "  <input type=\"submit\" name=\"execute_rename\" value=\" 実行 \">\n".
		       "  <input type=\"hidden\" name=\"action\" value=\"RENAME\">".
		       "  <input type=\"hidden\" name=\"lastmodified\" value=\"".&Util::escapeHTML($time)."\">\n".
		       "  <input type=\"hidden\" name=\"page\" value=\"".&Util::escapeHTML($page)."\">".
		       "</form>\n";
	}
}

1;
