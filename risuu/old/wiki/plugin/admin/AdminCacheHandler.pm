###############################################################################
#
# キャッシュの設定を行う管理画面
#
###############################################################################
package plugin::admin::AdminCacheHandler;
use strict;

#==============================================================================
# コンストラクタ
#==============================================================================
sub new {
	my $class = shift;
	my $self = {};
	return bless $self,$class;
}

#==============================================================================
# アクションハンドラメソッド
#==============================================================================
sub do_action {
	my $self  = shift;
	my $wiki  = shift;
	my $cgi   = $wiki->get_CGI();
	
	$wiki->set_title("バックアップ");
	
	if($cgi->param("SAVE") eq ""){
		return $self->form($wiki);
	} else {
		$self->save($wiki);
	}
}

#==============================================================================
# 設定フォーム
#==============================================================================
sub form {
	my $self   = shift;
	my $wiki   = shift;
	my $config = &Util::load_config_hash($wiki,'cache.dat');
	
	my $no_cache = $config->{no_cache};
	$no_cache =~ s/\t/\n/g;
	
	my $remove_cache = $config->{remove_cache};
	$remove_cache =~ s/\t/\n/g;
	
	$wiki->set_title("キャッシュの設定");
	
	# テンプレートにパラメータをセット
	my $tmpl = HTML::Template->new(filename=>$wiki->config('tmpl_dir')."/admin_cache.tmpl",
	                               die_on_bad_params => 0);
	
	$tmpl->param({
		USE_CACHE    => $config->{use_cache},
		NO_CACHE     => $no_cache,
		REMOVE_CACHE => $remove_cache
	});
	
	return "<form action=\"".$wiki->config('script_name')."\" method=\"POST\">\n".
	       $tmpl->output().
	       "<input type=\"hidden\" name=\"action\" value=\"ADMINCACHE\">\n";
	       "</form>\n";
}

#==============================================================================
# 設定を保存
#==============================================================================
sub save {
	my $self = shift;
	my $wiki = shift;
	my $cgi  = $wiki->get_CGI();
	
	my $use_cache    = $cgi->param('use_cache');
	my $no_cache     = $cgi->param('no_cache');
	my $remove_cache = $cgi->param('remove_cache');
	
	# 改行コードを変換
	$no_cache     =~ s/\r\n/\n/g;
	$no_cache     =~ s/\r/\n/g;
	$no_cache     =~ s/\n/\t/g;
	$remove_cache =~ s/\r\n/\n/g;
	$remove_cache =~ s/\r/\n/g;
	$remove_cache =~ s/\n/\t/g;
	
	my $config = {
		use_cache    => $use_cache,
		no_cache     => $no_cache,
		remove_cache => $remove_cache
	};
	
	&Util::save_config_hash($wiki,'cache.dat',$config);
	$wiki->redirectURL($wiki->config('script_name')."?action=ADMINCACHE");
}

1;
