###############################################################################
#
# YukiWikiの書式をサポートするフォーマットプラグイン
#
###############################################################################
package plugin::format::YukiWikiFormat;
use strict;
#==============================================================================
# コンストラクタ
#==============================================================================
sub new {
	my $class = shift;
	my $self  = {};
	return bless $self,$class;
}

#==============================================================================
# FSWikiの書式に変換します。
#==============================================================================
sub convert_to_fswiki {
	my $self   = shift;
	my $source = shift;
	
	my @lines  = split(/\n/,$source);
	my $buf    = "";
	
	foreach my $line (@lines){
		if($line =~ /^\*\*\*/){
			$buf .= "!".$self->_convert_line(substr($line,3))."\n";
		} elsif($line =~ /^\*\*/){
			$buf .= "!!".$self->_convert_line(substr($line,2))."\n";
		} elsif($line =~ /^\*/){
			$buf .= "!!!".$self->_convert_line(substr($line,1))."\n";
		} elsif($line =~ /^----/){
			$buf .= "----\n";
		} elsif($line =~ /^---/){
			$buf .= "***".$self->_convert_line(substr($line,3))."\n";
		} elsif($line =~ /^--/){
			$buf .= "**".$self->_convert_line(substr($line,2))."\n";
		} elsif($line =~ /^-/){
			$buf .= "*".$self->_convert_line(substr($line,1))."\n";
		} elsif($line =~ /^>/){
			$buf .= "\"\"".$self->_convert_line(substr($line,1))."\n";
		} elsif($line =~ /^[ \t]/){
			$buf .= $line."\n";
		} else {
			$buf .= $self->_convert_line($line)."\n";
		}
	}
	return $buf;
}

#==============================================================================
# インライン書式をFSWikiの書式に変換します。
#==============================================================================
sub convert_to_fswiki_line {
	my $self = shift;
	return $self->_convert_line(@_);
}

#==============================================================================
# インライン書式をYukiWikiの書式に変換します。
#==============================================================================
sub convert_from_fswiki_line {
	my $self = shift;
	return $self->_convert_line(@_);
}

#==============================================================================
# １行分のインライン書式を変換します。'''→''に、''→'''に変換にします。
#==============================================================================
sub _convert_line {
	my $self = shift;
	my $line = shift;
	my $buf  = "";
	
	if($line =~ /(''')(.+?)(''')/){
		my $pre   = $`;
		my $post  = $';
		my $label = $2;
		if($pre ne ""){ $buf .= $self->_convert_line($pre); }
		$buf .= "''$label''";
		if($post ne ""){ $buf .= $self->_convert_line($post); }
		
	} elsif($line =~ /('')(.+?)('')/){
		my $pre   = $`;
		my $post  = $';
		my $label = $2;
		if($pre ne ""){ $buf .= $self->_convert_line($pre); }
		$buf .= "'''$label'''";
		if($post ne ""){ $buf .= $self->_convert_line($post); }
	} else {
		$buf .= $line;
	}
	return $buf;
}

#==============================================================================
# FSWikiの書式から変換します。
#==============================================================================
sub convert_from_fswiki {
	my $self   = shift;
	my $source = shift;
	my @lines  = split(/\n/,$source);
	my $buf    = "";
	
	foreach my $line (@lines){
		if($line =~ /^!!!/){
			$buf .= "*".$self->_convert_line(substr($line,3))."\n";
		} elsif($line =~ /^!!/){
			$buf .= "**".$self->_convert_line(substr($line,2))."\n";
		} elsif($line =~ /^!/){
			$buf .= "***".$self->_convert_line(substr($line,1))."\n";
		} elsif($line eq "----"){
			$buf .= "----\n";
		} elsif($line =~ /^\*\*\*/){
			$buf .= "---".$self->_convert_line(substr($line,3))."\n";
		} elsif($line =~ /^\*\*/){
			$buf .= "--".$self->_convert_line(substr($line,2))."\n";
		} elsif($line =~ /^\*/){
			$buf .= "-".$self->_convert_line(substr($line,1))."\n";
		} elsif($line =~ /^""/){
			$buf .= ">".$self->_convert_line(substr($line,2))."\n";
		} elsif($line =~ /^[ \t]/){
			$buf .= $line."\n";
		} else {
			$buf .= $self->_convert_line($line)."\n";
		}
	}
	return $buf;
}

1;
