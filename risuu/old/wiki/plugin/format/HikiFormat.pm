###############################################################################
#
# Hikiの書式をサポートするフォーマットプラグイン
#
###############################################################################
package plugin::format::HikiFormat;
use strict;
#==============================================================================
# コンストラクタ
#==============================================================================
sub new {
	my $class = shift;
	my $self  = {};
	return bless $self,$class;
}

#==============================================================================
# FSWikiの書式に変換します。
#==============================================================================
sub convert_to_fswiki {
	my $self   = shift;
	my $source = shift;
	
	my @lines  = split(/\n/,$source);
	my $buf    = "";
	
	foreach my $line (@lines){
		if($line =~ /^!!!/){
			$buf .= "!".$self->convert_to_fswiki_line(substr($line,3))."\n";
		} elsif($line =~ /^!!/){
			$buf .= "!!".$self->convert_to_fswiki_line(substr($line,2))."\n";
		} elsif($line =~ /^!/){
			$buf .= "!!!".$self->convert_to_fswiki_line(substr($line,1))."\n";
		} elsif($line =~ /^###/){
			$buf .= "+++".$self->convert_to_fswiki_line(substr($line,3))."\n";
		} elsif($line =~ /^##/){
			$buf .= "++".$self->convert_to_fswiki_line(substr($line,2))."\n";
		} elsif($line =~ /^#/){
			$buf .= "+".$self->convert_to_fswiki_line(substr($line,1))."\n";
		} elsif($line =~ /^\|\|/){
			my @words = split(/\|\|/,$line);
			foreach my $word (@words){
				if($word ne ""){
					$buf .= ",".$self->convert_to_fswiki_line($word);
				}
			}
			$buf .= "\n";
		} elsif($line =~ /^[ \t]/){
			$buf .= $line."\n";
		} else {
			$buf .= $self->convert_to_fswiki_line($line)."\n";
		}
	}
	return $buf;
}

#==============================================================================
# インライン書式をHiki→FSWikiフォーマットに変換します。
#==============================================================================
sub convert_to_fswiki_line {
	my $self = shift;
	my $line = shift;
	my $buf  = "";
	
	# 別名リンク
	if($line =~ /\[\[([^\[]+?)\|((http|https|ftp|mailto):[a-zA-Z0-9\.,%~^_+\-%\/\?\(\)!\$&=:;\*#\@']*)\]\]/
	    ||  $line =~ /\[\[([^\[]+?)\|(file:[^\[\]]*)\]\]/
	    ||  $line =~ /\[\[([^\[]+?)\|((\/|\.\/|\.\.\/)+[a-zA-Z0-9\.,%~^_+\-%\/\?\(\)!\$&=:;\*#\@']*)\]\]/){
		my $pre   = $`;
		my $post  = $';
		my $label = $1;
		my $url   = $2;
		if($pre ne ""){ $buf .= $self->convert_to_fswiki_line($pre); }
		$buf .= "[$label|$url]";
		if($post ne ""){ $buf .= $self->convert_to_fswiki_line($post); }
		
	} else {
		$buf .= $line;
	}
	return $buf;
}

#==============================================================================
# FSWikiの書式から変換します。
#==============================================================================
sub convert_from_fswiki {
	my $self   = shift;
	my $source = shift;
	my @lines  = split(/\n/,$source);
	my $buf    = "";
	
	foreach my $line (@lines){
		if($line =~ /^!!!/){
			$buf .= "!".$self->convert_from_fswiki_line(substr($line,3))."\n";
		} elsif($line =~ /^!!/){
			$buf .= "!!".$self->convert_from_fswiki_line(substr($line,2))."\n";
		} elsif($line =~ /^!/){
			$buf .= "!!!".$self->convert_from_fswiki_line(substr($line,1))."\n";
		} elsif($line =~ /^\+\+\+/){
			$buf .= "###".$self->convert_from_fswiki_line(substr($line,3))."\n";
		} elsif($line =~ /^\+\+/){
			$buf .= "##".$self->convert_from_fswiki_line(substr($line,2))."\n";
		} elsif($line =~ /^\+/){
			$buf .= "#".$self->convert_from_fswiki_line(substr($line,1))."\n";
		} elsif($line =~ /^,/){
			my @words = map {/^"(.*)"$/ ? scalar($_ = $1, s/""/"/g, $_) : $_}
			                ($line =~ /,\s*("[^"]*(?:""[^"]*)*"|[^,]*)/g);
			foreach my $word (@words){
				if($word ne ""){
					$buf .= "||".$self->convert_from_fswiki_line($word);
				}
			}
			$buf .= "||\n";
		} elsif($line =~ /^[ \t]/){
			$buf .= $line."\n";
		} else {
			$buf .= $self->convert_from_fswiki_line($line)."\n";
		}
	}
	return $buf;
}

#==============================================================================
# インライン書式をFSWiki→Hikiフォーマットに変換します。
#==============================================================================
sub convert_from_fswiki_line {
	my $self = shift;
	my $line = shift;
	my $buf  = "";
	
	# 別名リンク
	if($line =~ /\[([^\[]+?)\|((http|https|ftp|mailto):[a-zA-Z0-9\.,%~^_+\-%\/\?\(\)!\$&=:;\*#\@']*)\]/
	    ||  $line =~ /\[([^\[]+?)\|(file:[^\[\]]*)\]/
	    ||  $line =~ /\[([^\[]+?)\|((\/|\.\/|\.\.\/)+[a-zA-Z0-9\.,%~^_+\-%\/\?\(\)!\$&=:;\*#\@']*)\]/){
		my $pre   = $`;
		my $post  = $';
		my $label = $1;
		my $url   = $2;
		if($pre ne ""){ $buf .= $self->convert_from_fswiki_line($pre); }
		$buf .= "[[$label|$url]]";
		if($post ne ""){ $buf .= $self->convert_from_fswiki_line($post); }
		
	# ページ別名リンク
	} elsif($line =~ /\[\[([^\[]+?)\|(.+?)\]\]/){
		my $pre   = $`;
		my $post  = $';
		my $label = $1;
		my $page  = $2;
		if($pre ne ""){ $buf .= $self->convert_from_fswiki_line($pre); }
		$buf .= "[[$label|$page]]";
		if($post ne ""){ $buf .= $self->convert_from_fswiki_line($post); }
		
	} else {
		$buf .= $line;
	}
	return $buf;
}

1;
