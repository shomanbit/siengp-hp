###############################################################################
#
# キャッシュ作成用HTMLパーサ
#
###############################################################################
package Wiki::CacheParser;
use Wiki::HTMLParser;
use vars qw(@ISA);
@ISA = qw(Wiki::HTMLParser);
#==============================================================================
# <p>
# コンストラクタ
# </p>
#==============================================================================
sub new {
	my $class = shift;
	my $wiki  = shift;
	my $self  = Wiki::HTMLParser->new($wiki,0);
	return bless $self,$class;
}

#==============================================================================
# <p>
# ヘッドライン
# </p>
#==============================================================================
sub l_headline {
	my $self  = shift;
	my $level = shift;
	my $obj   = shift;
	my $wiki  = $self->{wiki};
	
	if($self->{para}==1){
		$self->{html} .= "</p>";
		$self->{para} = 0;
	}
	
	$self->end_list;
	$self->end_verbatim;
	$self->end_table;
	$self->end_quote;
	
	my $html  = join("",@$obj);

	$self->{html} .= "<h".($level+1)."><!--p=".$self->{p_cnt}."-->".$html."<!--/p--></h".($level+1).">\n";
	$self->{p_cnt}++;
}

#==============================================================================
# <p>
# プラグイン
# </p>
#==============================================================================
sub plugin {
	my $self   = shift;
	my $plugin = shift;
	
	my $html = "\n<!--inline_plugin=".$plugin->{command}."-->\n";
	foreach my $arg (@{$plugin->{args}}){
		$html .= "  <!--arg=$arg-->\n";
	}
	$html .= "<!--/inline_plugin-->\n";
	
	return ($html);
}

#==============================================================================
# <p>
# パラグラフプラグイン
# </p>
#==============================================================================
sub l_plugin {
	my $self   = shift;
	my $plugin = shift;
	
	if($self->{para}==1){
		$self->{html} .= "</p>";
		$self->{para} = 0;
	}
	
	$self->end_list;
	$self->end_verbatim;
	$self->end_table;
	$self->end_quote;
	
	my $html = "\n<!--paragraph_plugin=".$plugin->{command}."-->\n";
	foreach my $arg (@{$plugin->{args}}){
		$html .= "  <!--arg=$arg-->\n";
	}
	$html .= "<!--/paragraph_plugin-->\n";

	if(defined($html) && $html ne ""){
		$self->{html} .= $html;
	}
}

#==============================================================================
# <p>
# キャッシュの動的部分を処理します。
# 他のモジュールから使用するため、メソッドではなく関数として実装しています。
# 以下のようにして使用します。
# </p>
# <pre>
# my $cache = Wiki::CacheParserで作成した形式のHTMLキャッシュ文字列;
# my $main  = メイン領域の場合1、サイドバーやヘッダ、フッタ領域の場合は0;
# my $html  = &amp;Wiki::CacheParser::process_cache($wiki,$cache,$main);
# </pre>
#==============================================================================
sub process_cache {
	my $wiki = shift;
	my $html = shift;
	my $main = shift;
	
	my $count  = 0;
	my $parser = Wiki::HTMLParser->new($wiki,$main);
	
	$html =~ s/\r//g;
	my @lines = split(/\n/,$html);
	$parser->{html} = "";
	
	for($count=0;$count<=$#lines;$count++){
		my $line = $lines[$count];
		# 見出しの処理
		if($line =~ /<!--p=([0-9]+?)-->/){
			if($main){
				$line =~ s/<!--p=([0-9]+?)-->/<a name="p$1">/;
				$parser->{p_cnt} = $1 + 1;
				$line =~ s/<!--\/p-->/<\/a>/;
			}
			$parser->{html} .= $line;
		# インラインプラグインの処理
		} elsif($line =~ /<!--inline_plugin=(.+?)-->/){
			my $plugin = {command=>$1,args=>[]};
			while(!($lines[$count] =~ /<!--\/inline_plugin-->/)){
				if($lines[$count] =~ /<!--arg=(.+?)-->/){
					push(@{$plugin->{args}},$1);
				}
				$count++;
			}
			my @plugin_result = $wiki->process_plugin($plugin,$parser);
			$parser->{html} .= join("",@plugin_result);
			
		# パラグラフプラグインの処理
		} elsif($line =~ /<!--paragraph_plugin=(.+?)-->/){
			my $plugin = {command=>$1,args=>[]};
			while(!($lines[$count] =~ /<!--\/paragraph_plugin-->/)){
				if($lines[$count] =~ /<!--arg=(.+?)-->/){
					push(@{$plugin->{args}},$1);
				}
				$count++;
			}
			my $plugin_result = $wiki->process_plugin($plugin,$parser);
			if(defined($plugin_result) && $plugin_result ne ""){
				$parser->{html} .= $plugin_result;
			}
		} else {
			$parser->{html} .= $line."\n";
		}
	}
	return $parser->{html};
}

1;
