<h3>最新情報</h3>
<p>
<a href="https://www.facebook.com/titech.riko.project" Target="_blank"><img src="./style/facebook-logo.png"></a>
<a href="https://twitter.com/titechspelec" Target="_blank"><img src="./style/twitter_logo.jpg"></a><br>
当プロジェクト・特別講義では、
<a href="https://www.facebook.com/titech.riko.project" Target="_blank">facebook</a>
及び
<a href="https://twitter.com/titechspelec" Target="_blank">Twitter</a>
公式アカウントより最新情報を随時お知らせしています。</p>
        