      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1 style="line-height: 1.18em;"><a href="index.html" title="トップへ戻ります">
	  <span style="font-size: 0.55em;">第8回シンポジウム</span><span style="font-size: 0.35em;">by 東京工業大学 理工系生能力発見・開発プロジェクト<br></span>
	  <span class="logo_colour"><span style="font-size: 1.32em;">トヨタの見据える未来</span>
	  <br><span style="font-size: 0.65em;">　　～次世代自動車戦略と燃料電池自動車～</span></span></a></h1>

      <div id="tweet_like">
      <!-- twitter_tweet -->
      <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://p.tl/BH_3" 
        data-text="#titechspelec 「トヨタの見据える未来」パネリスト：トヨタ自動車岡島氏・高橋氏 2014年1月24（金）18:00～＠東工大・大岡山キャンパス西9号館2Fデジタル多目的ホール" 
        data-count="horizontal" data-via="titechspelec">
      Tweet
      </a> 
      <!-- facebook_like -->
      <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftitech.riko.project&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:130px; height:21px;" allowTransparency="true; float:left"></iframe>
      </div>
      </div>
      </div>
