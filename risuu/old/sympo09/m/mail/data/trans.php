<html>
<head>
<title>ExportCSVdata</title>
	<?php
	$filepointer=fopen("./data.dat", "r");
	$filepointer2=fopen("./data.csv", "w");
	flock($filepointer, LOCK_EX);
	flock($filepointer2, LOCK_EX);
	rewind($filepointer2);
	rewind($filepointer);
//	fputs($filepointer2,mb_convert_encoding("名前,フリガナ,性別,年齢,所属,学年,E-Mail,質問・意見,登録日時,IPアドレス\n","SJIS","SJIS"));
	fputs($filepointer2,"名前,ふりがな,メールアドレス,性別,年齢,学校名,学部,学科,学年,会社,質問内容,登録日時,IPアドレス\n");
	while(!feof($filepointer)){
		$fileline=fgets($filepointer);
		if($fileline){
		list($name,$kana,$email,$sex,$age,$school,$gakubu,$gakka,$grade,$company,$question,$date,$ip)=mb_split(",",$fileline);
/*		switch($gender){
		case "1":
			$gender="男";
			break;
		case "2":
			$gender="女";
			break;
		default:
			$gender="error";
		}
		switch($age){
		case "1":
			$age="-10代";
			break;
		case "2":
			$age="20代";
			break;
		case "3":
			$age="30代";
			break;
		case "4":
			$age="40代";
			break;
		case "5":
			$age="50代";
			break;
		case "6":
			$age="60代-";
			break;
		default:
			$age="error";
		}
		switch($job){
		case "1":
			$job="高校生";
			break;
		case "2":
			$job="学生（東工大）";
			break;
		case "3":
			$job="学生（東京芸大）";
			break;
		case "4":
			$job="学生（その他）";
			break;
		case "5":
			$job="大学教員";
			break;
		case "6":
			$job="社会人（技術系）";
			break;
		case "7":
			$job="社会人（芸術系）";
			break;
		case "8":
			$job="社会人（その他）";
			break;
		case "9":
			$job="その他";
			break;
		default:
			$job="error";
		}
		switch($grade){
		case "1":
			$grade="高1";
			break;
		case "2":
			$grade="高2";
			break;
		case "3":
			$grade="高3";
			break;
		case "4":
			$grade="B1";
			break;
		case "5":
			$grade="B2";
			break;
		case "6":
			$grade="B3";
			break;
		case "7":
			$grade="B4";
			break;
		case "8":
			$grade="M1";
			break;
		case "9":
			$grade="M2";
			break;
		case "10":
			$grade="D1";
			break;
		case "11":
			$grade="D2";
			break;
		case "12":
			$grade="D3";
			break;
		default:
		}*/
//		$name=mb_convert_encoding($name,"SJIS","SJIS");
//		$kana=mb_convert_encoding($kana,"SJIS","SJIS");
//		$gender=mb_convert_encoding($gender,"SJIS","SJIS");
//		$age=mb_convert_encoding($age,"SJIS","SJIS");
//		$job=mb_convert_encoding($job,"SJIS","SJIS");
//		$grade=mb_convert_encoding($grade,"SJIS","SJIS");
//		$mail=mb_convert_encoding($mail,"SJIS","SJIS");
//		$message=mb_convert_encoding($message,"SJIS","SJIS");
		fputs($filepointer2,$name.",".$kana.",".$mail.",".$sex.",".$age.",".$school.",".$gakubu.",".$gakka.","$question",".$date.",".$ip);		 
		}
	}
	flock($filepointer, LOCK_UN);
	flock($filepointer2, LOCK_UN);
	fclose($filepointer);
	fclose($filepointer2);
	print("<meta http-equiv=\"Refresh\" content=\"0;URL=data.csv\">");
?>
</head>
<body>
自動でダウンロードが開始されない場合<a href="data.csv" type="text/csv">こちら</a><br />
<a href="javascript:window.close();">ウィンドウを閉じる</a>
</body>
</html>