<?php

// 受け取る時のSubject（件名）
$mailSubject = 'シンポジウム予約登録';

//送信メッセージ
$mailMessage = <<< EOD
以下の内容をフォームより受け取りました。
────────────────────────────────────
■氏名
{$sfm_mail->name}

■ふりがな
{$sfm_mail->kana}

■メールアドレス
{$sfm_mail->email}

■性別
{$sfm_mail->sex}

■年齢
{$sfm_mail->age}

■所属学校(学生の方)
{$sfm_mail->school} {$sfm_mail->gakubu} {$sfm_mail->gakka}
{$sfm_mail->grade}年

■勤務先(社会人の方)
{$sfm_mail->company}

■質問内容
{$sfm_mail->question}

────────────────────────────────────
□ユーザー情報
$sfm_userinfo
EOD;

?>
