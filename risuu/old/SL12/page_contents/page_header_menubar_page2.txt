      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index.html">トップ</a></li>
          <li><a href="page1.html">開催概要</a></li>
          <li class="selected"><a href="page2.html">講師</a></li>
          <li><a href="page3.html">アクセス</a></li>
          <li><a href="page4.html">申し込み</a></li>
          <li><a href="page5.html">理工系プロジェクトとは</a></li>
	  <li><a href="page6.html">お問い合わせ</a></li>
        </ul>
      </div>
