      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1 style="line-height: 1.18em;"><a href="index.html" title="トップへ戻ります">
	  <span style="font-size: 0.55em;">第７回特別講義　　</span><span style="font-size: 0.35em;">by 東京工業大学 理工系学生能力発見・開発プロジェクト<br></span>
	  <span class="logo_colour">　<span style="font-size: 1.32em;">こんな会社で働きたい!!</span>
	  <br><span style="font-size: 0.65em;">　　　　　　―良い会社を見極める方法とは―</span></span></a></h1>

      <div id="tweet_like">
      <!-- twitter_tweet -->
      <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://t.co/jCMeY5Up" 
        data-text="#titechspelec 「こんな会社で働きたい！！－良い会社を見極める方法とは－」講師：法政大学　坂本光司教授　1月15（火）18:00～＠東工大・大岡山キャンパス 蔵前会館 ロイヤルブルーホール" 
        data-count="horizontal" data-via="titechspelec">
      Tweet
      </a> 
      <!-- facebook_like -->
      <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftitech.riko.project&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:130px; height:21px;" allowTransparency="true; float:left"></iframe>
      </div>
      </div>
      </div>