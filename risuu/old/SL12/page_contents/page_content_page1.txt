      <div id="content">
        <!-- insert the page content here -->
	<h1><span style="font-size: 1.2em;">こんな会社で働きたい!!</span><br>
	<span style="font-size: 0.7em;"><B>　 　　―良い会社を見極める方法とは―</B></span>
	</h1>
<p>
　みなさんは、普段ニュースや新聞等で耳にする会社が、
<font color="Blue"><B>実際にはどのような経営・雇用を行っているか</B></font>考えてみたことはありますか？
例えば中小企業であっても、従業員がいきいきと仕事に励み、世界中から注文が殺到している企業もあります。<br>
　今回講義をしていただく坂本光司先生は、日本中の企業のコンサルティングに携わった経験を通して、
良い会社を見極める方法を提案されています。
今回の講義では顧客だけでなく、
従業員など会社に関わる人すべてにとって<font color="Blue"><B>「良い」会社を判断するための「ものさし」</B></font>についてお話しいただきます。
</p>

        <h1>日時・会場・講師</h1>
	<blockquote style="MARGIN-RIGHT: 0px" dir="ltr">
        <p><span style="font-size: 1.4em;">日時</span><br>
	<span style="font-size: 1.2em;">2013年1月15日(火)</span><br>
	17:30 : 開場<br>18:00～19:00 : ご講演<br>19:00～19:30 : 質疑応答<br>19:30 : 終了<br>
	定員：120名ほど<br>参加費：無料！！
	</p>
	<p><span style="font-size: 1.4em;">会場</span><br>
	<span style="font-size: 1.3em;">東京工業大学大岡山キャンパス　蔵前会館　ロイアルブルーホール</span><br>
	（詳しくは<a href="./page3.html">アクセス</a>のページをご覧ください。）
	</p>
	<p><span style="font-size: 1.4em;">講師</span><br>
	<strong><span style="font-size: 1.3em;">坂本 光司　氏</span></strong>
	<br>法政大学大学院政策創造研究科教授
	<br>（詳しくは<a href="./page2.html">講師</a>のページをご覧ください。）
	</p>
	</blockquote>

<!--
	<h1>当日のプログラム</h1>
	<blockquote style="MARGIN-RIGHT: 0px" dir="ltr">
	<p>17:30～：開場
	<br>18:00～19:00：坂本光司先生 講演
	<br>19:00～19:30：質疑応答
	<br>
	<br>
	<br>
	</p>
	</blockquote>
--!>
      </div>