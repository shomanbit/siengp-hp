<html>
<head>
<title>理数2008シンポ　参加登録状況</title>
</head>
<body>
<h2>登録状況</h2>
<a href="trans.php" target="_blank">データダウンロード(CSV形式)</a>
<table border=1 >
<?php
	$filepointer=fopen("./data.dat", "r");
	flock($filepointer, LOCK_EX);
	rewind($filepointer);
	print("<tr><td>名前</td><td>フリガナ</td><td>性別</td><td>年齢</td><td>所属</td><td>学年</td><td>E-Mail</td><td>質問・意見</td><td>登録日時</td><td>IPアドレス</td></tr>");
	while(!feof($filepointer)){
		$fileline=fgets($filepointer);
		if($fileline){
				list($name,$kana,$gender,$age,$job,$grade,$mail,$message,$date,$ip)=mb_split(",",$fileline);
				
		switch($gender){
		case "1":
			$gender="男";
			break;
		case "2":
			$gender="女";
			break;
		default:
			$gender="error";
		}
		switch($age){
		case "1":
			$age="-10代";
			break;
		case "2":
			$age="20代";
			break;
		case "3":
			$age="30代";
			break;
		case "4":
			$age="40代";
			break;
		case "5":
			$age="50代";
			break;
		case "6":
			$age="60代-";
			break;
		default:
			$age="error";
		}
		switch($job){
		case "1":
			$job="高校生";
			break;
		case "2":
			$job="学生（東工大）";
			break;
		case "3":
			$job="学生（東京芸大）";
			break;
		case "4":
			$job="学生（その他）";
			break;
		case "5":
			$job="大学教員";
			break;
		case "6":
			$job="社会人（技術系）";
			break;
		case "7":
			$job="社会人（芸術系）";
			break;
		case "8":
			$job="社会人（その他）";
			break;
		case "9":
			$job="その他";
			break;
		default:
			$job="error";
		}
		switch($grade){
		case "1":
			$grade="高1";
			break;
		case "2":
			$grade="高2";
			break;
		case "3":
			$grade="高3";
			break;
		case "4":
			$grade="B1";
			break;
		case "5":
			$grade="B2";
			break;
		case "6":
			$grade="B3";
			break;
		case "7":
			$grade="B4";
			break;
		case "8":
			$grade="M1";
			break;
		case "9":
			$grade="M2";
			break;
		case "10":
			$grade="D1";
			break;
		case "11":
			$grade="D2";
			break;
		case "12":
			$grade="D3";
			break;
		default:
		}
//		$name=mb_convert_encoding($name,"SJIS","SJIS");
//		$kana=mb_convert_encoding($kana,"SJIS","SJIS");
//		$gender=mb_convert_encoding($gender,"SJIS","SJIS");
//		$age=mb_convert_encoding($age,"SJIS","SJIS");
//		$job=mb_convert_encoding($job,"SJIS","SJIS");
//		$grade=mb_convert_encoding($grade,"SJIS","SJIS");
//		$mail=mb_convert_encoding($mail,"SJIS","SJIS");
//		$message=mb_convert_encoding($message,"SJIS","SJIS");
		print("<tr>");
		print("<td>".htmlspecialchars($name)."</td><td>".htmlspecialchars($kana)."</td><td>".$gender."</td><td>".$age."</td><td>".$job."</td><td>".$grade."</td><td>"."<a href=\"mailto:".$mail."\">".htmlspecialchars($mail)."</a></td><td>".htmlspecialchars($message)."</td><td>".$date."</td><td>".$ip."</td>");
		print("</tr>\n");
		}
	}
	flock($filepointer, LOCK_UN);
	fclose($filepointer);
	?>
</table>
</body>
</html>