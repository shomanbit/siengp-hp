<?php
print("<?xml version=\"1.0\" encoding=\"Shift_JIS\"?>");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<title>「万能人」　観覧登録フォーム</title>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link href="style.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<div id="container">
<div id="menu">
<img src="logo.jpg" width="220" height="220" class="menupic" /><br />
	<div class="menubox"><a href="index.html">トップページ</a></div>
	<div class="menubox"><a href="concept.html">コンセプト</a></div>
	<div class="menubox"><a href="panelist.html">パネリスト</a></div>
	<div class="menubox"><a href="access.html">アクセス</a></div>
	<div class="menubox"><a href="reserve.html">観覧申し込み</a></div>
	<div class="menubox"><a href="about.html">プロジェクトについて</a></div>
	<div class="menubox"><a  href="mailto:matsuoka.y.ac-sympo@m.titech.ac.jp?subject=お問い合わせ">お問い合わせ</a></div>
	<div class="space"></div>
携帯電話向けページもあります。<br />下のQRコードからどうぞ。<br />
<img src="qr.png" width="104" height="104" class="qr" /><br />
<div class="space"></div>
	<div class="linkbox">
		<h3 class="linkbox">関連リンク</h3><br />
		<ul>
		<li><a href="http://www.titech.ac.jp/">・東京工業大学</a></li>
		<li><a href="http://www.cradle.titech.ac.jp/elite/index.html">・理工系学生能力発見・開発<br />　プロジェクト</a></li>
		<li><a href="http://www.mext.go.jp/a_menu/jinzai/koubo/06122815.htm">・文部科学省<br />　理数学生応援プロジェクト</a></li>
		</ul>
	</div>
	<div class="staff"><a href="data/staff.php">スタッフ専用</a></div>
</div>
<div id="bg">
<div id="header">
文部科学省「理数学生応援プロジェクト」採択<br />
東京工業大学　理工系学生能力発見・開発プロジェクト<br />
第2回シンポジウム
</div>
<div id="main">
<h4>シンポジウム</h4>
<h2>万能人</h2>
<h3>-その生態と実態-</h3>
<div class="space10"></div>
<?php
	$name=$_POST['name'];
	$kana=$_POST['kana'];
	$gender=$_POST['gender'];
	$age=$_POST['age'];
	$job=$_POST['job'];
	$grade=$_POST['grade'];
	$mail=$_POST['mail'];
	$message=$_POST['message'];
	$auth=$_POST['auth1']+$_POST['auth2'];
	$count = 0;
	if(!$name){
		print("名前が入力されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	if(!$kana){
		print("フリガナが入力されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	if(!$gender){
		print("性別が選択されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	if(!$age){
		print("年齢が選択されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	if(!$job){
		print("所属が選択されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	if($auth!=10){
		print("数字が間違っているか入力されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	
	if($count==0){
		write();
		sqlwrite();
			print("登録ありがとうございました。ご来場お待ちしております。<br />");		
	}
	else{
		print("<a href=\"javascript:history.back()\">前ページへ戻る</a><br />");
	}
	
function write(){
	global $name;
	global $kana;
	global $gender;
	global $age;
	global $job;
	global $grade;
	global $mail;
	global $message;
//	$name_enc = mb_detect_encoding($name);
//	$kana_enc = mb_detect_encoding($kana);
//	$mail_enc = mb_detect_encoding($mail);
//	$message_enc = mb_detect_encoding($message);

	$filepointer=fopen("./data/data.dat", "a+");
	flock($filepointer, LOCK_EX);
	fputs($filepointer, 
//	mb_ereg_replace(",","，",mb_convert_encoding($name,"SJIS",$name_enc)).",".
//	mb_ereg_replace(",","，",mb_convert_encoding($kana,"SJIS",$kana_enc)).",".
mb_ereg_replace(",","，",$name).",".
mb_ereg_replace(",","，",$kana).",".
	$gender.",".
	$age.",".
	$job.",".
	$grade.",".
//	mb_ereg_replace(",","，",mb_convert_encoding($mail,"SJIS",$mail_enc)).",".
	$mail.",".
//	mb_ereg_replace(",","，",mb_ereg_replace("\n"," ",mb_ereg_replace("\r","",mb_convert_encoding($message,"SJIS",$message_enc)))).",".
	mb_ereg_replace(",","，",mb_ereg_replace("\n"," ",mb_ereg_replace("\r","",$message))).",".
	date("Y/m/d H:i:s").",".
	$_SERVER["REMOTE_ADDR"]."\n");
	flock($filepointer, LOCK_UN);
	fclose($filepointer);

}


function sqlwrite(){
	
}

?>
</div>

<div id="footer">
(C)2008　東京工業大学　理工系学生能力発見・開発プロジェクト All rights reserved.<br />
コンテンツの無断転載はご遠慮下さい。
</div>
</div>
</div>
</body>
</html>