<html>
<head>
<title>万能人 -東工大理数プロジェクトシンポジウム</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body><center>
シンポジウム「万能人」<br />-その生態と実態-<br />
<hr />
<?php
	$name=$_POST['name'];
	$kana=$_POST['kana'];
	$gender=$_POST['gender'];
	$age=$_POST['age'];
	$job=$_POST['job'];
	$grade=$_POST['grade'];
	$mail=$_POST['mail'];
	$message=$_POST['message'];
	$auth=$_POST['auth1'];
	$count = 0;
	if(!$name){
		print("名前が入力されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	if(!$kana){
		print("フリガナが入力されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	if(!$gender){
		print("性別が選択されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	if(!$age){
		print("年齢が選択されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	if(!$job){
		print("所属が選択されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	if($auth!=10){
		print("数字が間違っているか入力されていません。前のページに戻って再度入力をお願いします。<br />");
		$count++;
	}
	
	if($count==0){
		write();
		sqlwrite();
		print("登録ありがとうございました。ご来場お待ちしております。<br />");
	}
	else{
		print("<a href=\"reserve.html\">登録フォームへ戻る</a><br />");
	}
	
function write(){
	global $name;
	global $kana;
	global $gender;
	global $age;
	global $job;
	global $grade;
	global $mail;
	global $message;
//	$name_enc = mb_detect_encoding($name);
//	$kana_enc = mb_detect_encoding($kana);
//	$mail_enc = mb_detect_encoding($mail);
//	$message_enc = mb_detect_encoding($message);

	$filepointer=fopen("../data/data.dat", "a+");
	flock($filepointer, LOCK_EX);
	fputs($filepointer, 
	mb_ereg_replace(",","，",$name).",".
	mb_ereg_replace(",","，",$kana).",".
	$gender.",".
	$age.",".
	$job.",".
	$grade.",".
	mb_ereg_replace(",","，",$mail).",".
	mb_ereg_replace(",","，",mb_ereg_replace("\n"," ",mb_ereg_replace("\r","",$message))).",".
	date("Y/m/d H:i:s").",".
	$_SERVER["REMOTE_ADDR"]."\n");
	flock($filepointer, LOCK_UN);
	fclose($filepointer);

}


function sqlwrite(){
	
}

?>
</body>
</html>