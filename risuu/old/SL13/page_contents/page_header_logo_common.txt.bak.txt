      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1 style="line-height: 1.18em;"><a href="index.html" title="トップへ戻ります">
	  <span style="font-size: 0.55em;">第8回特別講義　　</span><span style="font-size: 0.35em;">by 東京工業大学 理工系学生能力発見・開発プロジェクト<br></span>
	  <span style="font-size: 1.32em;">定説を疑う<br>~疑問を抱くことが進化の第一歩~</span>

	  <br>

          <div id="tweet_like">
      	  <!-- twitter_tweet -->
          <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://t.co/jCMeY5Up" 
          data-text="#titechspelec 「定説を疑う ~疑問を抱くことが進化の第一歩~」講師：東京理科大学　渡辺正教授　12月19（火）17:00～＠東工大・大岡山キャンパス 西9号館2Fディジタル多目的ホール 
          data-count="horizontal" data-via="titechspelec">
          Tweet
          </a> 
          <!-- facebook_like -->
          <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ftitech.riko.project&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:130px; height:21px;" allowTransparency="true; float:left"></iframe>
      </div>
      </div>
      </div>
